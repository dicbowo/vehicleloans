-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2016 at 12:52 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id_account` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `auth_key` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id_account`, `username`, `password`, `role`, `token`, `auth_key`) VALUES
(1, 'pemeriksa1', 'f49159d5b103c5dec48594e54a53744f', 'Pemeriksa1', '', ''),
(2, 'pemeriksa2', '08c69ed230b37dc2ee95f4fd6b5a89b5', 'Pemeriksa2', '', ''),
(3, 'manager', '1d0258c2440a8d19e716292b231e3190', 'Manager', '', ''),
(4, 'customer1', 'ffbc4675f864e0e9aab8bdf7a0437010', 'Customer', '', ''),
(12, 'didik31', 'e35a7c67066bc13a30085e972e38a000', 'Customer', 'TictZVxn4h', '');

-- --------------------------------------------------------

--
-- Table structure for table `credit_propose`
--

CREATE TABLE `credit_propose` (
  `id_credit_propose` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `propose_date` date NOT NULL,
  `propose_status` varchar(100) NOT NULL,
  `approval_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_propose`
--

INSERT INTO `credit_propose` (`id_credit_propose`, `id_customer`, `propose_date`, `propose_status`, `approval_date`) VALUES
(1, 2, '2016-10-09', 'Approved - Step 3', '2016-10-09'),
(2, 2, '2016-10-09', 'Approved - Step 2', '2016-10-09'),
(3, 2, '2016-10-09', 'Rejected - Step 1', '2016-10-09');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL,
  `id_account` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `salary` decimal(16,0) NOT NULL,
  `transcript_attachment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `id_account`, `name`, `city`, `country`, `email`, `salary`, `transcript_attachment`) VALUES
(2, 4, 'Didik Wibowo', 'Semarang', 'Indonesia', 'dicbowo@gamail.com', '10000000', '-');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id_employee` int(11) NOT NULL,
  `id_account` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id_employee`, `id_account`, `name`, `city`, `country`, `email`) VALUES
(1, 1, 'Dummy Pemeriksa Satu', 'Semarang', 'Indonesia', 'dicbowo@gmail.com'),
(2, 2, 'Dummy Pemeriksa Dua', 'Semarang', 'Indonesia', 'dicbowo@gmail.com'),
(3, 3, 'Dummy Manager', 'Semarang', 'Indonesia', 'dicbowo@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id_account`);

--
-- Indexes for table `credit_propose`
--
ALTER TABLE `credit_propose`
  ADD PRIMARY KEY (`id_credit_propose`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id_employee`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id_account` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `credit_propose`
--
ALTER TABLE `credit_propose`
  MODIFY `id_credit_propose` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id_employee` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
