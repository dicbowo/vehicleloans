<?php

namespace app\controllers;

use Yii;
use app\models\Customer;
use app\models\CustomerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update','delete', 'loaddata', 'savedata'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Customer();

        if ($model->load(Yii::$app->request->post())) {

            $randomString = Yii::$app->getSecurity()->generateRandomString(5);
            $token = Yii::$app->getSecurity()->generateRandomString(10);
            $nama_array = explode(" ",$model->name);
            $unique = date("s");

            $account = new \app\models\Account();            
            $account->username = strtolower($nama_array[0]).$unique;
            $account->password = md5($randomString);
            $account->role = "Customer";
            $account->token = $token;
            $account->save();

            $model->id_account = $account->id_account;
            $model->transcript_attachment = "-";
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id_customer]);
            }
            else{
                return var_dump($model->errors);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionLoaddata(){
        if(isset($_POST['url'])){
            $url = $_POST['url'];
            $load = file_get_contents($url);
            $data = json_decode($load);
            die(json_encode(array('note' => 'loaded', 'object' => $data->records)));
        }
    }

    public function actionSavedata(){
        $name = explode(",", $_POST['name']);
        $city = explode(",", $_POST['city']);
        $country = explode(",", $_POST['country']);
        $email = explode(",", $_POST['email']);
        $salary = explode(",", $_POST['salary']);
        $saved = FALSE;

        for($i = 0; $i < count($name); $i++){
            $model = new Customer();

            $randomString = Yii::$app->getSecurity()->generateRandomString(5);
            $token = Yii::$app->getSecurity()->generateRandomString(10);
            $nama_array = explode(" ",$model->name);
            $unique = date("s");

            $account = new \app\models\Account();            
            $account->username = strtolower($nama_array[0] != '' ? $nama_array[0]:$model->name).$unique;
            $account->password = md5($randomString);
            $account->role = "Customer";
            $account->token = $token;
            $account->save();

            $model->id_account = $account->id_account;
            $model->transcript_attachment = "-";
            $model->name = $name[$i];
            $model->city = $city[$i];
            $model->country = $country[$i];
            $model->email = $email[$i];
            $model->salary = $salary[$i];

            if($model->save()){
                $this->sendaccount($model->id_customer);
                $saved = TRUE;
            }
            else{
                $saved = FALSE;
            }
        }

        if($saved){
            die(json_encode(array('x' => 1, 'note' => 'success')));
        }
        else{
            die(json_encode(array('x' => 0, 'note' => 'failed')));
        }
    }

    public function Sendaccount($id){
        $customer = \app\models\Customer::find()->where(['id_customer' => $id])->one();
        $account = \app\models\Account::find()->where(['id_account' => $customer->id_account])->one();

        $link = \yii\helpers\Url::to('@web/account/confirmation/' . $account->id_account.'?token='.$account->token, true);

        try {
            $body = '<!DOCTYPE html>
                    <html>
                    <head>
                    </head>
                    <body>
                        Dear '.$customer->name.',<br><br>
                        <p> Akun Anda telah dibuat. Silahkan konfimasi akun dengan klik link berikut:</p>
                        <p>'.$link.'</p>
                        <p>Terima Kasih</p>
                    </body>
                </html>';
            $mail = new \PHPMailer(true);
            $mail->IsSMTP();
            $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
            $mail->SMTPAuth = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
            $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
            $mail->Port = 587;                   // set the SMTP port for the GMAIL server
            $mail->Username = "dummydidik@gmail.com";  // GMAIL username
            $mail->Password = "exyfhlpkhjewyktm";            // GMAIL password
            $mail->AddAddress($customer->email, $customer->email);
            $mail->SetFrom('no-reply@didik.net', 'no-reply@didik.net');
            $mail->Subject = "Account Confirmation";
            $mail->MsgHTML($body);
            $mail->Send();
        } catch (phpmailerException $e) {
            
        } catch (Exception $e) {
            
        }
    }

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_customer]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
