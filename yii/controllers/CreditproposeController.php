<?php

namespace app\controllers;

use Yii;
use app\models\CreditPropose;
use app\models\CreditProposeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CreditproposeController implements the CRUD actions for CreditPropose model.
 */
class CreditproposeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CreditPropose models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CreditProposeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPropose(){
        $model = new CreditPropose();
        $id_account = \Yii::$app->user->identity->id_account;
        $customer = \app\models\Customer::find()->where(['id_account' => $id_account])->one();

        $model->id_customer = $customer->id_customer;
        $model->propose_date = date("Y-m-d");
        $model->propose_status = 'Request';
        $model->approval_date = '0000-00-00';
        if($model->save()){
            return $this->redirect(['index']);
        }
        return $this->redirect(['index']);
    }

    public function actionApproval($id){
        $model = $this->findModel($id);
        $role = \Yii::$app->user->identity->role;
        if($model->propose_status == 'Request' && $role == 'Pemeriksa1'){
            $model->propose_status = 'Approved - Step 1';
            $model->approval_date = date("Y-m-d");
        }
        elseif($model->propose_status == 'Approved - Step 1' && $role == 'Pemeriksa2'){
            $model->propose_status = 'Approved - Step 2';
            $model->approval_date = date("Y-m-d");
        }
        elseif($model->propose_status == 'Approved - Step 2' && $role == 'Manager'){
            $model->propose_status = 'Approved - Step 3';
            $model->approval_date = date("Y-m-d");
            $this->notification($model->id_credit_propose);
        }
        
        if($model->save()){
            return $this->redirect(['index']);
        }
        return $this->redirect(['index']);
    }

    public function actionReject($id){
        $model = $this->findModel($id);
        $role = \Yii::$app->user->identity->role;
        if($model->propose_status == 'Request' && $role == 'Pemeriksa1'){
            $model->propose_status = 'Rejected - Step 1';
            $model->approval_date = date("Y-m-d");
        }
        elseif($model->propose_status == 'Approved - Step 1' && $role == 'Pemeriksa2'){
            $model->propose_status = 'Rejected - Step 2';
            $model->approval_date = date("Y-m-d");
        }
        elseif($model->propose_status == 'Approved - Step 2' && $role == 'Manager'){
            $model->propose_status = 'Rejected - Step 3';
            $model->approval_date = date("Y-m-d");
        }
        
        if($model->save()){
            return $this->redirect(['index']);
        }
        return $this->redirect(['index']);
    }

    public function Notification($id){
        $propose = $this->findModel($id);
        $customer = \app\models\Customer::find()->where(['id_customer' => $propose->id_customer])->one();
        try {
            $body = '<!DOCTYPE html>
                    <html>
                    <head>
                    </head>
                    <body>
                        Dear '.$customer->name.',<br><br>
                        <p>Pengajuan kredit kendaraan Anda telah di terima.</p>
                        <p>Terima Kasih</p>
                    </body>
                </html>';
            $mail = new \PHPMailer(true);
            $mail->IsSMTP();
            $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
            $mail->SMTPAuth = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
            $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
            $mail->Port = 587;                   // set the SMTP port for the GMAIL server
            $mail->Username = "dummydidik@gmail.com";  // GMAIL username
            $mail->Password = "exyfhlpkhjewyktm";            // GMAIL password
            $mail->AddAddress($customer->email, $customer->email);
            $mail->SetFrom('no-reply@didik.net', 'no-reply@didik.net');
            $mail->Subject = "Account Confirmation";
            $mail->MsgHTML($body);
            $mail->Send();
        } catch (phpmailerException $e) {
            
        } catch (Exception $e) {
            
        }
    }

    /**
     * Displays a single CreditPropose model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CreditPropose model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CreditPropose();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_credit_propose]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CreditPropose model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_credit_propose]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CreditPropose model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CreditPropose model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CreditPropose the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CreditPropose::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
