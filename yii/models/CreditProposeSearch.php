<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CreditPropose;

/**
 * CreditProposeSearch represents the model behind the search form about `app\models\CreditPropose`.
 */
class CreditProposeSearch extends CreditPropose
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_credit_propose', 'id_customer'], 'integer'],
            [['propose_date', 'propose_status', 'approval_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CreditPropose::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_credit_propose' => $this->id_credit_propose,
            'id_customer' => $this->id_customer,
            'propose_date' => $this->propose_date,
            'approval_date' => $this->approval_date,
        ]);

        $query->andFilterWhere(['like', 'propose_status', $this->propose_status]);

        return $dataProvider;
    }
}
