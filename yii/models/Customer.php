<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id_customer
 * @property integer $id_account
 * @property string $name
 * @property string $city
 * @property string $country
 * @property string $email
 * @property string $salary
 * @property string $transcript_attachment
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_account', 'name', 'city', 'country', 'email', 'salary'], 'required'],
            [['id_customer', 'id_account'], 'integer'],
            [['salary'], 'number'],
            [['name', 'transcript_attachment'], 'string', 'max' => 255],
            [['city', 'country', 'email'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_customer' => 'Id Customer',
            'id_account' => 'Id Account',
            'name' => 'Name',
            'city' => 'City',
            'country' => 'Country',
            'email' => 'Email',
            'salary' => 'Salary',
            'transcript_attachment' => 'Transcript Attachment',
        ];
    }
}
