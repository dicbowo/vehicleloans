<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "credit_propose".
 *
 * @property integer $id_credit_propose
 * @property integer $id_customer
 * @property string $propose_date
 * @property string $propose_status
 * @property string $approval_date
 */
class CreditPropose extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'credit_propose';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_customer', 'propose_date', 'propose_status', 'approval_date'], 'required'],
            [['id_credit_propose', 'id_customer'], 'integer'],
            [['propose_date', 'approval_date'], 'safe'],
            [['propose_status'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_credit_propose' => 'Id Credit Propose',
            'id_customer' => 'Id Customer',
            'propose_date' => 'Propose Date',
            'propose_status' => 'Propose Status',
            'approval_date' => 'Approval Date',
        ];
    }
}
