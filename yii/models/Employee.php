<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property integer $id_employee
 * @property integer $id_account
 * @property string $name
 * @property string $city
 * @property string $country
 * @property string $email
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_account', 'name', 'city', 'country', 'email'], 'required'],
            [['id_employee', 'id_account'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['city', 'country', 'email'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_employee' => 'Id Employee',
            'id_account' => 'Id Account',
            'name' => 'Name',
            'city' => 'City',
            'country' => 'Country',
            'email' => 'Email',
        ];
    }
}
