<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Account */
/* @var $form yii\widgets\ActiveForm */

$listrole = [
	'Pemeriksa1' => 'Pemeriksa 1',
	'Pemeriksa2' => 'Pemeriksa 2',
	'Manager' => 'Manager',
	'Customer' => 'Customer',
];
?>

<div class="account-form">

	<div  class="x_panel">
		<div class="x_title">
			<h2><?= Html::encode($this->title) ?></h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<?php $form = ActiveForm::begin(); ?>

		    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

		    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

		    <?= $form->field($model, 'role')->dropDownList($listrole) ?>

		    <div class="form-group">
		        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>
		</div>
	</div>

</div>
