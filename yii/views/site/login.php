<?php 
use yii\helpers\Url;

$id_username = "u".uniqid();
$id_password = "p".uniqid();
$id_form = "f".uniqid();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentellela Alela! | </title>

    <!-- Bootstrap -->
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form id="<?= $id_form ?>" method='post' action="#" onsubmit="javascript: return false;">
              <h1>Login Form</h1>
              <div>
                <input type="text" class="form-control" name="username" id="<?= $id_username ?>" placeholder="Username" required="" />
              </div>
              <div>
                <input type="password" class="form-control" name="password" id="<?= $id_password ?>" placeholder="Password" required="" />
              </div>
              <div id="loading" class="progress" style="display: none;">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%;">Please wait
                </div>
              </div>
              <div id="alert" class="alert alert-danger alert-dismissible fade in" role="alert" style="display: none;">
                Incorrect username or password
              </div>
              <div>
                <button class="btn btn-default submit" type="sumbit">Log in</button>
                <a class="reset_pass" href="#">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-car"></i> Vehicle Loans</h1>
                  <p>©2016 Vehicle Loans - Didik Wibowo</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form>
              <h1>Create Account</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <a class="btn btn-default submit" href="index.html">Submit</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-car"></i> Vehicle Loans</h1>
                  <p>©2016 Vehicle Loans - Didik Wibowo</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
<script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script>
  jQuery(document).ready(function () {
    $('#<?= $id_form ?>').submit(function(e){
      $('#alert').hide();
      e.preventDefault();
      var _u = $('#<?= $id_username ?>').val();
      var _p = $('#<?= $id_password ?>').val();

      var formdata = {
          username: _u,
          password: _p,
          _csrf: '<?= Yii::$app->request->getCsrfToken() ?>'

      };

      $('#loading').slideDown('fast');

      $.post('<?= Url::to(['site/ajaxlogin']) ?>', formdata, function (data) {
          var parsed = JSON.parse(data);
          if (parsed.userlogin == '1') {
              $('#loading').slideUp('slow');
              top.location.href = '<?= Url::to(['site/home']) ?>';
          }
          else {

              $('#loading').slideUp('fast', function(){
                  $('#alert').show();
              });
          }
      });
    });
  });
</script>
