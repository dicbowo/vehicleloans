<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">
    <div class="row">
        <div class="col-xs-4">
            <div  class="x_panel">
                <div class="x_title">
                    <h2><?= Html::encode($this->title) ?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'salary')->textInput(['maxlength' => true]) ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <div class="col-xs-8">
            <div class="row">
                <div class="col-xs-12">
                    <div  class="x_panel">
                        <div class="x_title">
                            <h2>Load Data Customer</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="form-group field-customer-name required">
                                <label class="control-label" for="customer-name">URL</label>
                                <input type="text" id="urldata" class="form-control" name="urldata">
                            </div>
                            <div class="form-group">
                                <a id="load" class="btn btn-success">Load</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Data Loaded</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <button class="btn btn-primary" id="clear"><i class="fa fa-refresh"></i></button>
                                </li>
                                <li>
                                    <button class="btn btn-success" id="save"><i class="fa fa-save"></i></button>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-bordered" id="tabledata">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>City</th>
                                        <th>Country</th>
                                        <th>Email</th>
                                        <th>Salary</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.name = [];
    window.city = [];
    window.country = [];
    window.email = [];
    window.salary = [];
    window.addEventListener('load', function(){
        window.loadcustomer = function(){
            $url = $('#urldata').val();
            $.post('<?= Url::to(["customer/loaddata"]) ?>',{
                _csrf: '<?= Yii::$app->request->getCsrfToken() ?>',
                url : $url,
            },function(data){
                var parsed = JSON.parse(data);
                var data = parsed.object;

                for(i=0; i< data.length; i++){
                    var html = $('#html_row').html();
                    html = html.substring(4, html.length - 3);
                    html = html.replace(new RegExp("_ID_", "g"), i);
                    $('#tabledata tbody').append(html);
                    if('Name' in data[i]){$('#name'+i+'').val(data[i]['Name']);}
                    if('City' in data[i]){$('#city'+i+'').val(data[i]['City']);}
                    if('Country' in data[i]){$('#contry'+i+'').val(data[i]['Country']);}
                    if('Email' in data[i]){$('#email'+i+'').val(data[i]['Email']);}
                    if('Salary' in data[i]){$('#salari'+i+'').val(data[i]['Salary']);}
                }
            });
        }

        $('#load').click(function(){
            loadcustomer();
        });

        window.savecustomer = function(){
            console.log('save data dipanggil');
            var name_ = [];
            var city_ = [];
            var country_ = [];
            var email_ = [];
            var salary_ = [];

            $('input[name="name"]').each(function () {
                name_.push($(this).val());
            });
            $('input[name="city"]').each(function () {
                city_.push($(this).val());
            });
            $('input[name="country"]').each(function () {
                country_.push($(this).val());
            });
            $('input[name="email"]').each(function () {
                email_.push($(this).val());
            });
            $('input[name="salary"]').each(function () {
                salary_.push($(this).val());
            });

            if(name_.length > 0 && city_.length > 0 && country_.length > 0 && email_.length > 0 && salary_.length > 0){
                console.log('proses ajax dimulai');
                $.post('<?= Url::to(["customer/savedata"]) ?>', {
                    _csrf: '<?= Yii::$app->request->getCsrfToken() ?>',
                    name : name_.join(","),
                    city : city_.join(","),
                    country : country_.join(","),
                    email : email_.join(","),
                    salary : salary_.join(","),
                }, function (data) {
                    console.log('feedback');
                    var parsed = JSON.parse(data);
                    if(parsed.x == 1){
                        $(function(){
                            new PNotify({
                                title: 'Data Customer',
                                text: 'Save data successfully',
                                type: 'info',
                                styling: 'bootstrap3',
                            });
                        });
                    }
                    else{
                        $(function(){
                            new PNotify({
                                title: 'Data Customer',
                                text: 'Save data failed',
                                type: 'danger',
                                styling: 'bootstrap3',
                            });
                        });
                    }
                });
            }
            else{
                $(function(){
                    new PNotify({
                        title: 'Data Customer',
                        text: 'Data Null',
                        type: 'info',
                        styling: 'bootstrap3',
                    });
                });
            }
        }

        $('#save').click(function(){
            savecustomer();
        });

        $('#clear').click(function(){
            $('#tabledata tbody').empty();
        });

        $('#tabledata').on('click', '.hapus', function () {
            if (confirm('Are you sure?')) {
                $(this).closest('tr').remove();
            }
        });
    });
</script>
<div id="html_row"><!--
    <tr id="_IT_">
        <td>
            <button class="btn btn-danger hapus"><i class="fa fa-times"></i></button>
        </td>
        <td><input type="text" class="form-control" name="name" id="name_ID_"></td>
        <td><input type="text" class="form-control" name="city" id="city_ID_"></td>
        <td><input type="text" class="form-control" name="country" id="contry_ID_"></td>
        <td><input type="text" class="form-control" name="email" id="email_ID_"></td>
        <td><input type="text" class="form-control" name="salary" id="salari_ID_"></td>        
</tr> -->
</div>
