<?php 
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\web\View;

$c = $this->context->id;

 ?>
 <!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php $this->title ?>Vehicle Loans</title>

    <!-- Bootstrap -->
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- PNotify -->
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo \yii::$app->view->theme->baseUrl; ?>/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="" class="site_title"><i class="fa fa-car"></i> <span>Vehicle Loans</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img src="<?php echo \yii::$app->view->theme->baseUrl; ?>/production/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?=\Yii::$app->user->identity->username?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>&nbsp;</h3>
              </div>
              <div class="menu_section">
                <ul class="nav side-menu">
                  <li class="<?= ($c == 'site') ? 'current-page' : '' ?>">
                    <a href="<?= Url::to(['site/index']) ?>">
                      <i class="fa fa-home"></i> Home</span>
                    </a>
                  </li>
                  <?php if(Yii::$app->user->identity->role == 'Manager'): ?>
                  <li class="<?= ($c == 'account') ? 'current-page' : '' ?>">
                    <a href="<?= Url::to(['account/index']) ?>">
                      <i class="fa fa-user"></i> Account</span>
                    </a>
                  </li>
                  <?php endif; ?>
                  <li class="<?= ($c == 'creditpropose') ? 'current-page' : '' ?>">
                    <a href="<?= Url::to(['creditpropose/index']) ?>">
                      <i class="fa fa-file"></i> Credit Propose</span>
                    </a>
                  </li>
                  <?php if(Yii::$app->user->identity->role != 'Customer'): ?>
                  <li class="<?= ($c == 'account') ? 'current-page' : '' ?>">
                    <a href="<?= Url::to(['customer/index']) ?>">
                      <i class="fa fa-user"></i> Customer</span>
                    </a>
                  </li>
                  <?php endif; ?>
                  <?php if(Yii::$app->user->identity->role != 'Customer'): ?>
                  <li class="<?= ($c == 'employee') ? 'current-page' : '' ?>">
                    <a href="<?= Url::to(['employee/index']) ?>">
                      <i class="fa fa-user"></i> Employee</span>
                    </a>
                  </li>
                  <?php endif; ?>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo \yii::$app->view->theme->baseUrl; ?>/production/images/img.jpg" alt=""><?=\Yii::$app->user->identity->username?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="#" onclick="$('#form-logout').submit();return false"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
              <form method="post" action="<?= Url::to(['site/logout']) ?>" id="form-logout">
                  <input type="hidden"name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>">
              </form>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div>
            <div class="row top_tiles">
              <?=
                Breadcrumbs::widget([
                    'options' => [
                        'class' => 'breadcrumb',
                    ],
                    'itemTemplate' => "<li> {link} </li>\n",
                    'homeLink' => [
                        'label' => Yii::t('yii', 'Home'),
                        'url' => Yii::$app->homeUrl,
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
              ?>
              <?= $content ?>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Didik Wibowo - Vehicle Loans &copy; 2016
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <!-- <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/fastclick/lib/fastclick.js"></script> -->
    <!-- NProgress -->
    <!-- <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/nprogress/nprogress.js"></script> -->
    <!-- Chart.js -->
    <!-- <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/Chart.js/dist/Chart.min.js"></script> -->
    <!-- jQuery Sparklines -->
    <!-- <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script> -->
    <!-- Flot -->
    <!-- <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/Flot/jquery.flot.resize.js"></script> -->
    <!-- Flot plugins -->
    <!-- <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/flot.curvedlines/curvedLines.js"></script> -->
    <!-- DateJS -->
    <!-- <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/DateJS/build/date.js"></script> -->
    <!-- bootstrap-daterangepicker -->
    <!-- <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/production/js/moment/moment.min.js"></script>
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/production/js/datepicker/daterangepicker.js"></script> -->
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo \yii::$app->view->theme->baseUrl; ?>/build/js/custom.min.js"></script>

    <script>
      
    </script>
  </body>
</html>