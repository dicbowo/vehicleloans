<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CreditPropose */

$this->title = 'Update Credit Propose: ' . $model->id_credit_propose;
$this->params['breadcrumbs'][] = ['label' => 'Credit Proposes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_credit_propose, 'url' => ['view', 'id' => $model->id_credit_propose]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="credit-propose-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
