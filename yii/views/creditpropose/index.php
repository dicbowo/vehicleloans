<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CreditProposeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Credit Proposes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credit-propose-index">
    <div  class="x_panel">
        <div class="x_title">
            <h2><?= Html::encode($this->title) ?></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <?php if(\Yii::$app->user->identity->role == 'Customer'): ?>
                    <p>
                        <?= Html::a('Propose Credit', ['propose'], ['class' => 'btn btn-success']) ?>
                    </p>
                    <?php endif; ?>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'group',
                        'header' => '&nbsp',
                        'format' => 'raw',
                        'value' => function($model) {
                            $view_url = Url::to(['creditpropose/view', 'id' => $model->id_credit_propose]);
                            $delete_url = Url::to(['creditpropose/delete', 'id' => $model->id_credit_propose]);
                            $approval_url = Url::to(['creditpropose/approval', 'id' => $model->id_credit_propose]);
                            $reject_url = Url::to(['creditpropose/reject', 'id' => $model->id_credit_propose]);
                            return '<div class="btn-group dropup">
                                      <button class="btn blue dropdown-toggle" type="button" data-toggle="dropdown">
                                        Action <i class="fa fa-angle-up"></i>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                        '.((\Yii::$app->user->identity->role != 'Customer')? ''.((substr($model->propose_status, 0, 8) != 'Rejected' && $model->propose_status != 'Approved - Step 3')? '<li><a href="#" onclick="top.location.href=\'' . $approval_url . '\';return false"><i class="fa fa-check-square"></i> Appove</a></li>':'').'
                                        '.(($model->propose_status != 'Approved - Step 3' && substr($model->propose_status, 0, 8) != 'Rejected')? '<li><a href="#" onclick="top.location.href=\'' . $reject_url . '\';return false"><i class="fa fa-close"></i> Reject</a></li>':'').'
                                        ':'').'
                                        <li><a href="#" onclick="top.location.href=\'' . $view_url . '\';return false"><i class="fa fa-eye"></i> View</a></li>
                                        '.((\Yii::$app->user->identity->role != 'Customer')? '<li><a href="#" onclick="top.location.href=\'' . $delete_url . '\';return false"><i class="fa fa-trash"></i> Delete</a></li>':'').'
                                      </ul>
                                    </div>';
                        }
                    ],
                    'propose_date',
                    'propose_status',
                    'approval_date',
                ],
            ]); ?>
        </div>
    </div>
</div>
