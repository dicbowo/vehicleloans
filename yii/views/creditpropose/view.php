<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CreditPropose */

$this->title = $model->id_credit_propose;
$this->params['breadcrumbs'][] = ['label' => 'Credit Proposes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credit-propose-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_credit_propose], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_credit_propose], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_credit_propose',
            'id_customer',
            'propose_date',
            'propose_status',
            'approval_date',
        ],
    ]) ?>

</div>
