<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CreditProposeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="credit-propose-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_credit_propose') ?>

    <?= $form->field($model, 'id_customer') ?>

    <?= $form->field($model, 'propose_date') ?>

    <?= $form->field($model, 'propose_status') ?>

    <?= $form->field($model, 'approval_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
