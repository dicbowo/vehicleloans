<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CreditPropose */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="credit-propose-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_credit_propose')->textInput() ?>

    <?= $form->field($model, 'id_customer')->textInput() ?>

    <?= $form->field($model, 'propose_date')->textInput() ?>

    <?= $form->field($model, 'propose_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'approval_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
