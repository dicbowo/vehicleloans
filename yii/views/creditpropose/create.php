<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CreditPropose */

$this->title = 'Create Credit Propose';
$this->params['breadcrumbs'][] = ['label' => 'Credit Proposes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credit-propose-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
